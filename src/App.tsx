import {
  Container,
  Divider,
  Grid,
  MenuItem,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";

type LanguagePack = { n: string; b: string; r: string; q: string; k: string };

function invertLangPack(
  langPack: LanguagePack
): Record<LanguagePack[keyof LanguagePack], keyof LanguagePack> {
  return Object.fromEntries(
    Object.entries(langPack).map(([k, v]) => [v, k])
  ) as ReturnType<typeof invertLangPack>;
}

const languages: Record<
  "Slovenščina" | "Angleščina" | "Figurska",
  LanguagePack
> = {
  Slovenščina: { n: "S", b: "L", r: "T", q: "D", k: "K" },
  Angleščina: { n: "N", b: "B", r: "R", q: "Q", k: "K" },
  Figurska: { n: "♘", b: "♗", r: "♖", q: "♕", k: "♔" },
} as const;

const invertedLanguages = Object.fromEntries(
  Object.entries(languages).map(([langKey, langPack]) => [
    langKey,
    invertLangPack(langPack),
  ])
) as Record<
  "Slovenščina" | "Angleščina" | "Figurska",
  Record<LanguagePack[keyof LanguagePack], keyof LanguagePack>
>;

const langKeys = Object.keys(languages) as LanguageKey[];

type LanguageKey = keyof typeof languages;

function LanguagePicker(props: {
  language: LanguageKey;
  setLanguage: (n: LanguageKey) => void;
  label: string;
}) {
  return (
    <TextField
      label={props.label}
      select
      value={props.language}
      onChange={(e) => props.setLanguage(e.target.value as LanguageKey)}
    >
      {langKeys.map((l) => (
        <MenuItem key={l} value={l}>
          {l}
        </MenuItem>
      ))}
    </TextField>
  );
}

function translate({
  pgn,
  langFrom,
  langTo,
}: {
  pgn: string;
  langFrom: LanguageKey;
  langTo: LanguageKey;
}): string {
  const bracket_tokens = pgn.split("]");

  const header_part = bracket_tokens
    .slice(0, bracket_tokens.length - 1)
    .join("]");

  const movetext = bracket_tokens[bracket_tokens.length - 1];

  const normalized_movetext = Object.entries(
    invertedLanguages[langFrom]
  ).reduce((acc, [k, v]) => {
    return acc
      .replaceAll(k.toLowerCase(), v.toUpperCase())
      .replaceAll(k.toUpperCase(), v.toUpperCase());
  }, movetext);

  const translated_movetext = Object.entries(languages[langTo]).reduce(
    (acc, [k, v]) => {
      return acc
        .replaceAll(k.toLowerCase(), v.toUpperCase())
        .replaceAll(k.toUpperCase(), v.toUpperCase());
    },
    normalized_movetext
  );

  if (header_part.trim() === "") {
    return translated_movetext;
  } else {
    return [header_part, translated_movetext].join("]");
  }
}

function App() {
  const [langFrom, setLangFrom] = useState<LanguageKey>("Angleščina");

  const [langTo, setLangTo] = useState<LanguageKey>("Slovenščina");

  const [pgn, setPgn] = useState("");

  const translated = translate({ pgn, langFrom, langTo });

  const displayable = translated.trim() === "]" ? "" : translated;

  return (
    <Container component={Paper} sx={{ p: 2, mt: 2 }}>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Stack spacing={1}>
            <Typography variant="h4" component={"h1"}>
              PGN prevajalnik
            </Typography>

            <Typography variant="body2" color="error">
              PGN naj bo brez variacij in komentarjev.
            </Typography>
            <Divider />
          </Stack>
        </Grid>

        <Grid item md={6} xs={12}>
          <Stack spacing={2}>
            <LanguagePicker
              label="Iz jezika"
              language={langFrom}
              setLanguage={(n) => setLangFrom(n)}
            />

            <TextField
              multiline
              minRows={12}
              value={pgn}
              onChange={(e) => setPgn(e.target.value)}
            />
          </Stack>
        </Grid>

        <Grid item md={6} xs={12}>
          <Stack spacing={2}>
            <LanguagePicker
              label="V jezik"
              language={langTo}
              setLanguage={(n) => setLangTo(n)}
            />

            <TextField
              multiline
              minRows={12}
              value={displayable}
              onChange={() => {}}
            />
          </Stack>
        </Grid>

        <Grid item xs={12}>
          <Stack spacing={1}>
            <Divider />

            <Typography variant="body2">
              Piškotkov in podobne navlake ta stran ne uporablja.
            </Typography>

            <Typography variant="body2">
              2023 © Matija Sirk, vse pravice pridržane.
            </Typography>
          </Stack>
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
